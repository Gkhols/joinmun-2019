class AddFirstCouncilPreferenceToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :first_council_preference, :string
  end
end
