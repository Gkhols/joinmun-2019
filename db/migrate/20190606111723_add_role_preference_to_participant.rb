class AddRolePreferenceToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :role_preference, :string
  end
end
