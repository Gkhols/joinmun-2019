class AddPaymentToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :payment, :string
  end
end
