class CreateParticipants < ActiveRecord::Migration[5.2]
  def change
    create_table :participants do |t|
      t.string :email
      t.string :fullname
      t.string :nationality
      t.string :institution
      t.string :phone_number
      t.string :package_choice

      t.timestamps
    end
  end
end
