class AddFilePathToPost < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :file_path, :string
  end
end
