class AddSecondRolePreferenceToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :second_role_preference, :string
  end
end
