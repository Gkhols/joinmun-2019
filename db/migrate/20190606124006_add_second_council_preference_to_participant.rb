class AddSecondCouncilPreferenceToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :second_council_preference, :string
  end
end
