class AddFooterToPost < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :footer, :string
  end
end
