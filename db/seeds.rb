# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
   users = User.create([
     {username: 'user1',password: 'aASDQ', name: "Chair of United Nations Development Programme" },
     {username: 'user2',password: 'aBDQW', name: "Chair of United Nations Development Programme" },
     {username: 'user3',password: 'aCSDA', name: "Chair of Disarmament and International Security Committee" },
     {username: 'user4',password: 'aSASQ', name: "Chair of Disarmament and International Security Committee" },
     {username: 'user5',password: 'aQQWE', name: "Chair of International Labour Organization" },
     {username: 'user6',password: 'aCCXZ', name: "Chair of International Labour Organization" },
     {username: 'user7',password: 'aPQQW', name: "Chair of Association of Southeast Asian Nations" },
     {username: 'user8',password: 'aLSSA', name: "Chair of Association of Southeast Asian Nations" },
     {username: 'user9',password: 'aASAA', name: "Chair of United Nations Security Council" },
     {username: 'user10',password: 'aASSD', name: "Chair of United Nations Security Council" },
     {username: 'user11',password: 'aPPLS', name: "Chair of International Criminal Tribunal for the former Yugoslavia" },
     {username: 'user12',password: 'aSQQA', name: "Chair of International Criminal Tribunal for the former Yugoslavia" },
     {username: 'user13',password: 'aDFGH', name: "Chair of Joint Crisis Committee" },
     {username: 'user14',password: 'aZXSW', name: "Chair of Joint Crisis Committee" },
     {username: 'user15',password: 'aQWER', name: "Chair of Joint Crisis Committee" },
     {username: 'user16',password: 'aGHYT', name: "Chair of Joint Crisis Committee" },
     {username: 'user17',password: 'aZMKI', name: "Chair of Joint Crisis Committee" },
     {username: 'user18',password: 'aPOIU', name: "Chair of Joint Crisis Committee" },
     {username: 'user19',password: 'aJHGF', name: "Chair of Press Corps" },
     {username: 'user20',password: 'aLKIO', name: "user" },
     {username: 'user21',password: 'aJJHG', name: "user" },
     {username: 'user22',password: 'aLLKJ', name: "user" },
     {username: 'user23',password: 'aPPOY', name: "user" },
     {username: 'user24',password: 'aMKIU', name: "user" },
     {username: 'user25',password: 'aKKJH', name: "user" },
     {username: 'user26',password: 'aGGHY', name: "user" },
     {username: 'user27',password: 'aHHSD', name: "user" },
     {username: 'user28',password: 'aCVXX', name: "user" },
     {username: 'user29',password: 'aXXZZ', name: "user" },
     {username: 'user30',password: 'aSSWW', name: "user" },
     {username: 'user31',password: 'aSSAD', name: "user" },
     {username: 'user32',password: 'aQQTT', name: "user" },
     {username: 'user33',password: 'aPLLU', name: "user" },
     {username: 'user34',password: 'aWWYY', name: "user" },
     {username: 'user35',password: 'aXSER', name: "user" },
     {username: 'user36',password: 'aDEWR', name: "user" },
     {username: 'user37',password: 'aUYTG', name: "user" },
     {username: 'user38',password: 'aUTEB', name: "user" }])
#   Character.create(name: 'Luke', movie: movies.first)
