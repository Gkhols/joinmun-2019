class ParticipantsController < ApplicationController
before_action :authenticate_user, only:[:show, :index]

  def index
    @participants = Participant.all

    render json: @participants
  end


  def show
    @showparticipant = Participant.where(email:params[:email]).limit(1)
    render json: @showparticipant
  end


  def create
    @participant = Participant.new(participant_params)
    if @participant.save
      UserMailer.welcome_email(@participant).deliver_now
      render json: @participant, status: :created
    else
      render json: @participant.errors, status: :unprocessable_entity
    end
  end


  def update
    @editparticipant = Participant.where(email: participant_params[:email])
    if @editparticipant.update(upload_params)
      @editparticipant.reload
      render json: @editparticipant
    else
      render json: @editparticipant.errors, status: :unprocessable_entity
    end
  end


  def destroy
    @delparticipant = Participant.find(params[:id])
    @delparticipant.destroy
    head 204
  end

  private
    # Use callbacks to share common setup or constraints between actions.

    # Only allow a trusted parameter "white list" through.
    def participant_params
      params.permit(:email, :fullname, :nationality, :institution, :phone_number, :package_choice, :payment, :first_council_preference, :role_preference, :second_council_preference, :second_role_preference)
    end

    def upload_params
      params.permit(:email, :payment)
    end
end
