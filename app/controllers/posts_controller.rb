class PostsController < ApplicationController
before_action :authenticate_user, only:[:create, :destroy, :update]
before_action :set_user, only: [ :create, :show, :update, :destroy]
  def index
    @posts = Post.all

    render json: @posts
  end

  def show
    @post = Post.find(params[:id])
    render json: @post
  end

  def create
    @userdid = post_params
    @userdid[:user_id] = current_user.id
    @post = Post.new(@userdid)
    if @post.save
      @post.reload
      render json: @post, status: :created
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  def update
    @post = Post.where(id:params[:id])
    if @post.update(judul:post_params[:judul], message:post_params[:message], footer:post_params[:footer])
      render json: @post
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    render json: @post
  end

  private

  def set_user
     @user = current_user
  end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.permit(:judul, :message, :footer, :user_id, :file_path)
    end
end
