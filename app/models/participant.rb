class Participant < ApplicationRecord
  validates :email, presence: true
  mount_uploader :payment, PaymentUploader
end
