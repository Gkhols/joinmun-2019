class UserMailer < ApplicationMailer
  def welcome_email(participant)
    attachments['joinmunpayment.pdf'] = open("https://res.cloudinary.com/nostorage/image/upload/v1564670232/samples/PAYMENT_DETAILS_JOINMUN_2019_-_LAST_WAVE.pdf"){ |f| f.read }
    @participant = participant
    mail(from:'JOINMUN 2019', to:@participant.email, subject:"JOINMUN 2019 PAYMENT METHOD")
  end

end
