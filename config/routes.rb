Rails.application.routes.draw do

  #resources :posts

  #participant
  post 'participant/daftar' => 'participants#create'
  post 'participant/upload' => 'participants#update'
  get 'participants' => 'participants#index'
  post 'participant/show' => 'participants#show'
  delete 'participant/del' => 'participants#destroy'

  post 'post/create' => 'posts#create'
  get 'post/all' => 'posts#index'
  post 'post/detail' => 'posts#show'
  post 'post/update' => 'posts#update'
  post 'post/delete' => 'posts#destroy'
  #hi account
  post 'user/daftar' => 'users#create'
  get 'users' => 'users#index'
  post 'user/show' => 'users#show'
  post 'user/del' => 'users#destroy'
  post 'user/login' => 'user_token#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
